<?php
class LotteryDrawServer {


  public function __construct()
  {  

    //Connect to the database using PDO
   $this->dbh = new PDO('mysql:host=localhost;dbname=lottery_game', 'root', '');
  }
  
  public function draw_numbers() { 
    $num_picked_numbers = 6;
    $numbers = 42;
    $winning_numbers = array(); 
    while(count($winning_numbers) < $num_picked_numbers) { 
      // set only unique number  
      $id = rand(1,$numbers); 
      // This will basically keep duplicates from being created.
      $winning_numbers[$id] = true;  
    } 
    foreach($winning_numbers as $z => $y) { 
    // Assigns keys to a new array
    $winning_numbers_arr[] = $z; 
    }
    $final_won_numbers = implode("-", $winning_numbers_arr);
    
    $sql_statements = $this->dbh->prepare('INSERT into winning_numbers (won_numbers) VALUES (?)');
    $sql_statements->bindValue(1, $final_won_numbers);
    
    //execute the sql statement
    $sql_statements->execute();
    
    //return  statement if success or fail
    if($sql_statements) {
      return $final_won_numbers;
    } else {
      return false;
    }
    
  }
  
  
  
}

?>
