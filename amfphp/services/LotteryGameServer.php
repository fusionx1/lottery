<?php
class LotteryGameServer {


  public function __construct()
  {  

    //Connect to the database using PDO
   $this->dbh = new PDO('mysql:host=localhost;dbname=lottery_game', 'root', '');
  }
  
   public function add_picked_numbers($picked_numbers)
  {
    date_default_timezone_set('Asia/Manila'); 
    
    $current_datetime = date("Y-m-d G:i:s");
    //time will start at 7am and end at 8pm
    $start_datetime = date("Y-m-d") . " 07:00:00";
    $end_datetime = date("Y-m-d") . " 20:00:00";
    
    $start_timestamp = strtotime($start_datetime);
    $end_timestamp = strtotime($end_datetime);
    $current_timestamp = strtotime($current_datetime);
    
    if((($current_timestamp >= $start_timestamp) && ($current_timestamp <= $end_timestamp))) {
      $sql_statements = $this->dbh->prepare('INSERT into lottery (picked_numbers, security_code, ticket_no) VALUES (?, ?, ?)');
      $security_code = substr(md5(uniqid(rand(), true)),0,6);
      $mode = 10;
      $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
      $salt = sprintf("$2a$%02d$", $mode) . $salt;
      $hashed_code = crypt($security_code, $salt);
      $ticket_no =  date('Y'). uniqid(); 
   
      //binding attributes
      $sql_statements->bindValue(1, $picked_numbers);
      $sql_statements->bindValue(2, $hashed_code);
      $sql_statements->bindValue(3, $ticket_no);
    
      //execute the sql statement
      $sql_statements->execute();
    
      //return  statement if success or fail
      if($sql_statements) {
        return $security_code.':'.$ticket_no.':'.'time_in_range';
      } else {
        return false;
      }
      
    } else {
        return "no_security_code:no_ticket:time_not_in_range";
    }
  
  }
  public function get_numbers()
  {
    //Query all numbers
    $query = $this->dbh->query("SELECT * FROM lottery ORDER BY date_added DESC");
    
    //Fetch all results
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    
    //Return all the results
    return $result;
  }
  
   public function check_numbers($ticket_no, $security_code) {
    
    $sth = $this->dbh->prepare('SELECT security_code, picked_numbers FROM lottery WHERE ticket_no = :ticket_no LIMIT 1');
    $sth->bindParam(':ticket_no', trim($ticket_no), PDO::PARAM_INT);
    $sth->execute();
    $results = $sth->fetch(); 
    $hashed_password = $results['security_code'];
    
    if (crypt(trim($security_code), trim($hashed_password)) == trim($hashed_password)) {
      // Authenticated  
      $hashed = "authenticated";
      
      ////////////////////////
      
      //Query all winning numbers
      $query = $this->dbh->query("SELECT won_numbers FROM winning_numbers");
      //Fetch all results
      $items = $query->fetchAll(PDO::FETCH_ASSOC);
      $status = "";
      foreach($items as $item) {
        $arr_won_numbers = explode("-", $item['won_numbers']);
      }
      $arr_picked_numbers = explode("-", $results['picked_numbers']);
      $diff_result = array_diff($arr_won_numbers,$arr_picked_numbers);
    
      if(count($diff_result) <=3){
        $status = "You win, here are the winning numbers \n". $item['won_numbers'];
      } else {
        $status = "Sorry, here are the winning numbers \n". $item['won_numbers'];
      }
      
      
      ///////////////////////
      
    } else {
      $hashed = "not_authenticated";
    }

    $picked_numbers = $results['picked_numbers'].":".$hashed.':'. $status;
    return $picked_numbers;
    
  }
  
  
  
  public function get_hashed_password($ticket_no) {
    $sth = $this->dbh->prepare('SELECT security_code, picked_numbers FROM lottery WHERE ticket_no = :ticket_no LIMIT 1');
    $sth->bindParam(':ticket_no', $ticket_no, PDO::PARAM_INT);
    $sth->execute();
    $results = $sth->fetch();
    //Return all the results
    return $results['security_code'];
  }
   
}

?>
